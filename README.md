# Консольная утилитка для объединения картинок в один многстраничный файл tiff

## Запуск

Если используете poetry:

1.  ```poetry shell```
2.  ```poetry install```
3.  ```python main.py``` либо ```python3 main.py```

Если используется pip

1. Создать виртуальное окружение ```python3 -m venv venv``` либо ```python -m venv venv```
2. Активировать виртуальное окружение ```source venv/bin/activate``` (на Ubuntu) либо ```cd venv\Scripts``` + ```./Activate.ps1``` (На Windows с помощью Powershell)
1. ```pip install -r requirements.txt```
2. ```python main.py``` либо ```python3 main.py```