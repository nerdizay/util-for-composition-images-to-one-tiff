from PIL import Image
from typing import List
from pathlib import Path
from enum import Enum
import math
import os


desktop = Path(os.path.expanduser('~')) / Path("Desktop")

class EndComposition(Exception):
    pass
    
class Oriantation(Enum):
    portret = "portret"
    landscape = "landscape"
    
convert_mm_to_300DPI_pixels = lambda mm: int(mm*300/25.4)
    
class Format:
    def __init__(
        self, width_mm: int,
        height_mm: int,
        name = "custom",
        orientation: Oriantation = Oriantation.portret.value
    ):
        self.name = name
        
        if (
            (orientation == Oriantation.portret.value and width_mm > height_mm) or
            (orientation == Oriantation.landscape.value and width_mm < height_mm)
        ):
            width_mm, height_mm = height_mm, width_mm
            
        self.width_mm = width_mm
        self.height_mm = height_mm
        
        self.width_pixels = convert_mm_to_300DPI_pixels(width_mm)
        self.height_pixels = convert_mm_to_300DPI_pixels(height_mm)
        
A4 = Format(210, 297, "A4", Oriantation.landscape.value)


def calc_count_images_on_each_side_page(
    size_image: tuple[int, int],
    page: Format = A4,
    non_printing_area_mm: int = 5,
    gap = 0
) -> tuple[int, int]:
    """ size_image - in pixels """
    
    gap = convert_mm_to_300DPI_pixels(gap)
    
    canvas_free_zone_width = page.width_pixels - convert_mm_to_300DPI_pixels(non_printing_area_mm)
    canvas_free_zone_height = page.height_pixels - convert_mm_to_300DPI_pixels(non_printing_area_mm)
    
    image_width_pixels = size_image[0]
    image_height_pixels = size_image[1]
    
    count_on_width = 0
    count_on_height = 0
    
    while canvas_free_zone_width > image_width_pixels:
        if count_on_width == 0:
            canvas_free_zone_width -= image_width_pixels
        else:
            canvas_free_zone_width = canvas_free_zone_width - (image_width_pixels + gap)
        
        count_on_width += 1
        
    while canvas_free_zone_height > image_height_pixels:
        if count_on_height == 0:
            canvas_free_zone_height -= image_height_pixels
        else:
            canvas_free_zone_height = canvas_free_zone_width - (image_height_pixels + gap)
        
        count_on_height += 1
        
    return (count_on_width, count_on_height)
        
def create_composition(
    folder: str,
    result_format: Format = A4,
    non_printing_area_mm: int = 5,
    gap = 5
) -> None:
    
    gap = convert_mm_to_300DPI_pixels(gap)
    
    images = []
    for img_path in Path(folder).rglob("*"):
        with open(img_path) as img:
            images.append(img)
        
    with Image.open(images[0].name) as image:
        count_width, count_height = calc_count_images_on_each_side_page(
            image.size,
            page=result_format,
            non_printing_area_mm=non_printing_area_mm,
            gap=gap
        )

    count_pages = math.ceil(len(images)/(count_height*count_width))
    
    general_image = Image.new('RGB', (result_format.width_pixels, result_format.height_pixels), (256, 256, 256))
    additionals = []
    
    try:
        for page in range(count_pages):
            
            if page == 0:
                new_image = general_image
            else:
                new_image = Image.new('RGB', (result_format.width_pixels, result_format.height_pixels), (256, 256, 256))
            
            current_begin_x = current_begin_y = begin_x = begin_y = convert_mm_to_300DPI_pixels(non_printing_area_mm)
            image = None
            image_width = None
            image_height = None
            
            for _ in range(count_width):
                for _ in range(count_height):
                    
                    try:
                        image = Image.open(images.pop().name)
                    except IndexError:
                        raise EndComposition
                        
                    image_width = image.size[0]
                    image_height = image.size[1]
                    
                    new_image.paste(
                        image,
                        (
                            current_begin_x,
                            current_begin_y,
                            current_begin_x + image_width,
                            current_begin_y + image_height,
                        )
                    )
                    
                    image.close()
                    
                    current_begin_y = current_begin_y + image_height + gap
                    
                current_begin_y = begin_y
                current_begin_x = current_begin_x + image_width + gap
                
            if page == 0:
                pass
            else:
                additionals.append(new_image)
                
    except EndComposition:
        additionals.append(new_image)
        
    general_image.save(desktop / Path("Result.tif"), "TIFF", save_all=True, append_images=additionals, quality=100, dpi=(300,300))



print("Введите путь до папки, в которой хранятся картинки")
path_to_images = input()
create_composition(Path(path_to_images))
print("Проверьте рабочий стол. Файл - Result.tif")